// possible poses in the game
var possible_poses = ['lift the left arm!',
             'lift the right arm!',
             'lower both arms.'];

// required pose by Ballant
var required_pose = "";

// initial values for pose
var armleft_up = 0;
var armright_up = 0;

// get a random pose from Ballant
function getPose(pose){
    return possible_poses[getRandomInt(possible_poses.length)];
};

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function sayCongrats(){
    // message to be displayed when we have a success
    document.getElementById("congrats").innerHTML = "Congratulations!";
}

function sayTryAgain(){
    // message to be displayed when we have to try again
    document.getElementById("congrats").innerHTML = "You have raised at least one arm. Try again.";
}

function changePose(){

    // first, we reset the result found in the `id="congrats"` paragraph.
    document.getElementById("congrats").innerHTML = "Try again.";

    // we get a new pose
    required_pose = getPose(possible_poses);
   
    // we reset the variables containing the present pose
    armleft_up = 0;
    armright_up = 0;
    // finally, we display the required pose in the `id="actions"` paragraph.
    document.getElementById("action").innerHTML = required_pose;
}

setInterval(changePose,3000);

var socket = io('http://127.0.0.1:8081');

socket.on('connect', function() {
        socket.emit('config',
              {
                    server: {
                    port: 9000,
                    host: 'localhost'
                },
                    client: {
                    port: 3334,
                    host: '127.0.0.1'
                }
            });
});

socket.on('message', function(obj) {
    // let's start by making sure we have an 'armup' filter. if this is the case...
    if (obj[0].includes("armup")) {

        // ... then we keep in memory which arm it is...
        var current_arm = obj[0];
        
        // ... and if the arm is raised or not.
        var current_status = obj[1];
        
        // if the arm is currently raised :
        if (current_status == 1){
        
            // if the left arm is currently raised...
            if (current_arm.includes('left')){

                // then we save this info in the variable 'armleft_up' 
                armleft_up = 1;
            
            // else, if it is the right arm that is raised...
            } else if (current_arm.includes('right')){
                // we save this info in the variable 'armright_up'
                armright_up = 1;
        
            }
        }
        // Action(s) to be taken according to the player arms position.
        if (required_pose == possible_poses[0]){

            // here, the game asked to raise your left arm.
            // if the left arm is up and the right arm is down,
            // then we display a success message.            
            if ( armleft_up == 1 && armright_up == 0 ){

                sayCongrats();
            }
    
        } else if (required_pose == possible_poses[1]){

            // otherwise, if the game asked to raise the right arm,
            // we display a success message if the right arm is raised
            // while the left arm is down.
            
            if ( armright_up == 1 && armleft_up == 0){

                sayCongrats();
            }
    
        } else if (required_pose == possible_poses[2]){

            // else, if we need to lower both arms,
            // then we display an error message as soon as one arm is
            // raised, or a success message if not.
            if ( armright_up == 1 || armleft_up == 1){
            
                sayTryAgain();

            } else {
            
                sayCongrats();
            }
        }
    }
});


