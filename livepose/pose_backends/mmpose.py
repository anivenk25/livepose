from mmcv.utils import env as mmcv_env
from mmpose.datasets.dataset_info import DatasetInfo
from mmpose.utils import StopWatch
from mmpose.apis import (get_track_id, inference_top_down_pose_model,
                         init_pose_model, vis_pose_result)
import logging
import os
import sys
from typing import Any, Dict, List, Optional
import re

import argparse
import time
from collections import deque
from queue import Queue
from threading import Event

import cv2
import torch
import numpy as np

from livepose.dataflow import Channel, Flow, Stream
from livepose.pose_backend import PosesForCamera, Pose, Keypoint, PoseBackend, register_pose_backend

logger = logging.getLogger(__name__)

CONFIG_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../../external/mmpose"
))

MODEL_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../models/mmpose"
))

# We package configs and models together:
if os.path.exists(os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../models/mmpose/configs"))):
    CONFIG_DIR = MODEL_DIR

ROOT_DIR = os.path.realpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "../.."
))


try:
    from mmdet.apis import inference_detector, init_detector
    has_mmdet = True
except (ImportError, ModuleNotFoundError) as e:
    has_mmdet = False

try:
    import psutil
    psutil_proc: Optional[psutil.Process] = psutil.Process()
except (ImportError, ModuleNotFoundError):
    psutil_proc = None


@register_pose_backend("mmpose")
class MMPoseBackend(PoseBackend):
    """
    mmpose backend
    """

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self._frame_buffer: Any
        self._input_queue: Any
        self._det_result_queue: Any
        self._pose_result_queue: Any
        self._det_model: Any
        self._pose_model_list: Any
        self._pose_history_list: Any
        self._event_exit: Any = Event()
        self._event_inference_done: Any = Event()

    def add_parameters(self) -> None:
        super().add_parameters()
        self.parse_args()

    def parse_parameters(self, *args: Any, **kwargs: Any) -> None:
        super().parse_parameters(*args, **kwargs)

        # Update mmpose config and model paths
        actions = self._parser._option_string_actions
        for action in list(actions):
            argument = actions[action]
            arg = argument.dest
            value = getattr(self._args, arg)
            if type(value) == str:
                openmmlab_urls = ["https://download.openmmlab.com/mmdetection/v2.0/",
                                  "https://download.openmmlab.com/mmpose/"]
                for url in openmmlab_urls:
                    if value.startswith(url):
                        value = os.path.realpath(os.path.join(MODEL_DIR, value.replace(url, '')))
                        setattr(self._args, arg, value)
                external_path = "external/mmpose/"
                external_pos = value.find(external_path)
                if external_pos > -1:
                    value = os.path.realpath(os.path.join(CONFIG_DIR, value[external_pos+len(external_path):]))
                    setattr(self._args, arg, value)

    def init(self) -> None:

        print("self._args.det_config", self._args.det_config)

        if self._args.enable_detection:
            assert has_mmdet, 'Please install mmdetection or set enable detection to 0.'
            assert self._args.det_config is not None
            assert self._args.det_checkpoint is not None

            # build detection model
            self._det_model = init_detector(
                self._args.det_config, self._args.det_checkpoint, device=self._args.device.lower())

        # build pose models
        self._pose_model_list = []
        if self._args.enable_human_pose:
            pose_model = init_pose_model(
                self._args.human_pose_config,
                self._args.human_pose_checkpoint,
                device=self._args.device.lower())
            model_info = {
                'name': 'HumanPose',
                'model': pose_model,
                'cat_ids': self._args.human_det_ids,
                'bbox_color': (148, 139, 255),
            }
            self._pose_model_list.append(model_info)

        if self._args.enable_animal_pose:
            pose_model = init_pose_model(
                self._args.animal_pose_config,
                self._args.animal_pose_checkpoint,
                device=self._args.device.lower())
            model_info = {
                'name': 'AnimalPose',
                'model': pose_model,
                'cat_ids': self._args.animal_det_ids,
                'bbox_color': 'cyan',
            }
            self._pose_model_list.append(model_info)

        if self._args.enable_hand_pose:
            pose_model = init_pose_model(
                self._args.hand_pose_config,
                self._args.hand_pose_checkpoint,
                device=self._args.device.lower())
            model_info = {
                'name': 'HandPose',
                'model': pose_model,
                'cat_ids': self._args.hand_det_ids,
                'bbox_color': 'cyan',
            }
            self._pose_model_list.append(model_info)

        # store pose history for pose tracking
        self._pose_history_list = []
        for _ in range(len(self._pose_model_list)):
            self._pose_history_list.append({'self._pose_results_last': [], 'next_id': 0})

        # frame buffer
        if self._args.buffer_size > 0:
            buffer_size = self._args.buffer_size
        else:
            # infer buffer size from the display delay time
            # assume that the maximum video fps is 30
            buffer_size = round(30 * (1 + max(self._args.display_delay, 0) / 1000.))
        self._frame_buffer = Queue(maxsize=buffer_size)

        # queue of input frames
        # element: (timestamp, frame)
        self._input_queue = deque(maxlen=1)

        # queue of detection results
        # element: tuple(timestamp, frame, time_info, self._det_results)
        self._det_result_queue = deque(maxlen=1)

        # queue of detection/pose results
        # element: (timestamp, time_info, self._pose_results_list)
        self._pose_result_queue = deque(maxlen=1)

        self._inference_detection_stop_watch = StopWatch(window=10)
        self._inference_pose_stop_watch = StopWatch(window=10)
        self._display_stop_watch = StopWatch(window=10)

    def parse_args(self) -> None:
        self._parser.add_argument('--cam-id', type=str, default='0')
        self._parser.add_argument(
            '--enable-detection',
            type=int,
            default=1,
            help='Enable detection of entities (persons, animals...) '
            'containing keypoints, represented with bounding boxes and labels')
        self._parser.add_argument(
            '--det-config',
            type=str,
            default=os.path.realpath(os.path.join(CONFIG_DIR,
                                                  'demo/mmdetection_cfg/ssdlite_mobilenetv2_scratch_600e_coco.py')),
            help='Config file for detection')
        self._parser.add_argument(
            '--det-checkpoint',
            type=str,
            default='https://download.openmmlab.com/mmdetection/v2.0/ssd/'
            'ssdlite_mobilenetv2_scratch_600e_coco/ssdlite_mobilenetv2_'
            'scratch_600e_coco_20210629_110627-974d9307.pth',
            help='Checkpoint file for detection')
        self._parser.add_argument(
            '--enable-human-pose',
            type=int,
            default=1,
            help='Enable human pose estimation')
        self._parser.add_argument(
            '--enable-animal-pose',
            type=int,
            default=0,
            help='Enable animal pose estimation')
        self._parser.add_argument(
            '--enable-hand-pose',
            type=int,
            default=0,
            help='Enable hand pose estimation')
        self._parser.add_argument(
            '--human-pose-config',
            type=str,
            default=os.path.realpath(os.path.join(CONFIG_DIR,
                                                  'configs/wholebody/2d_kpt_sview_rgb_img/topdown_heatmap/',
                                                  'coco-wholebody/vipnas_res50_coco_wholebody_256x192_dark.py')),
            help='Config file for human pose')
        self._parser.add_argument(
            '--human-pose-checkpoint',
            type=str,
            default='https://download.openmmlab.com/'
            'mmpose/top_down/vipnas/'
            'vipnas_res50_wholebody_256x192_dark-67c0ce35_20211112.pth',
            help='Checkpoint file for human pose')
        self._parser.add_argument(
            '--human-det-ids',
            type=int,
            default=[1],
            nargs='+',
            help='Object category label of human in detection results.'
            'Default is [1(person)], following COCO definition.')
        self._parser.add_argument(
            '--animal-pose-config',
            type=str,
            default=os.path.realpath(os.path.join(CONFIG_DIR,
                                                  'configs/animal/2d_kpt_sview_rgb_img/topdown_heatmap/',
                                                  'animalpose/hrnet_w32_animalpose_256x256.py')),
            help='Config file for animal pose')
        self._parser.add_argument(
            '--animal-pose-checkpoint',
            type=str,
            default='https://download.openmmlab.com/mmpose/animal/hrnet/'
            'hrnet_w32_animalpose_256x256-1aa7f075_20210426.pth',
            help='Checkpoint file for animal pose')
        self._parser.add_argument(
            '--animal-det-ids',
            type=int,
            default=[16, 17, 18, 19, 20],
            nargs='+',
            help='Object category label of animals in detection results'
            'Default is [16(cat), 17(dog), 18(horse), 19(sheep), 20(cow)], '
            'following COCO definition.')
        self._parser.add_argument(
            '--hand-pose-config',
            type=str,
            default=os.path.realpath(os.path.join(CONFIG_DIR,
                                                  'configs/hand/2d_kpt_sview_rgb_img/topdown_heatmap/',
                                                  'onehand10k/res50_onehand10k_256x256.py')),
            help='Config file for hand pose')
        self._parser.add_argument(
            '--hand-pose-checkpoint',
            type=str,
            default='https://download.openmmlab.com/'
            'mmpose/top_down/resnet/'
            'res50_onehand10k_256x256-e67998f6_20200813.pth',
            help='Checkpoint file for hand pose')
        self._parser.add_argument(
            '--hand-det-ids',
            type=int,
            default=[],
            nargs='+',
            help='Object category label of hand in detection results.'
            'Default is [1(hand)], following COCO definition.')
        self._parser.add_argument(
            '--device', default='cuda:0', help='Device used for inference')
        self._parser.add_argument(
            '--det-score-thr',
            type=float,
            default=0.5,
            help='bbox score threshold')
        self._parser.add_argument(
            '--kpt-thr', type=float, default=0.3, help='bbox score threshold')
        self._parser.add_argument(
            '--vis-mode',
            type=int,
            default=2,
            help='0-none. 1-detection only. 2-detection and pose.')
        self._parser.add_argument(
            '--sunglasses', action='store_true', help='Apply `sunglasses` effect.')
        self._parser.add_argument(
            '--bugeye', action='store_true', help='Apply `bug-eye` effect.')

        self._parser.add_argument(
            '--out-video-file',
            type=str,
            default=None,
            help='Record the video into a file. This may reduce the frame rate')

        self._parser.add_argument(
            '--out-video-fps',
            type=int,
            default=20,
            help='Set the FPS of the output video file.')

        self._parser.add_argument(
            '--buffer-size',
            type=int,
            default=-1,
            help='Frame buffer size. If set -1, the buffer size will be '
            'automatically inferred from the display delay time. Default: -1')

        self._parser.add_argument(
            '--inference-fps',
            type=int,
            default=10,
            help='Maximum inference FPS. This is to limit the resource consuming '
            'especially when the detection and pose model are lightweight and '
            'very fast. Default: 10.')

        self._parser.add_argument(
            '--display-delay',
            type=int,
            default=0,
            help='Delay the output video in milliseconds. This can be used to '
            'align the output video and inference results. The delay can be '
            'disabled by setting a non-positive delay time. Default: 0')

        self._parser.add_argument(
            '--synchronous-mode',
            action='store_true',
            help='Enable synchronous mode that video I/O and inference will be '
            'temporally aligned. Note that this will reduce the display FPS.')

    def process_mmdet_results(self, mmdet_results, class_names=None, cat_ids=1) -> List[Dict]:
        """Process mmdet results to mmpose input format.

        Args:
            mmdet_results: raw output of mmdet model
            class_names: class names of mmdet model
            cat_ids (int or List[int]): category id list that will be preserved
        Returns:
            List[Dict]: detection results for mmpose input
        """
        if isinstance(mmdet_results, tuple):
            mmdet_results = mmdet_results[0]

        if not isinstance(cat_ids, (list, tuple)):
            cat_ids = [cat_ids]

        # only keep bboxes of interested classes
        bbox_results = [mmdet_results[i - 1] for i in cat_ids]
        bboxes = np.vstack(bbox_results)

        # get textual labels of classes
        labels = np.concatenate([
            np.full(bbox.shape[0], i - 1, dtype=np.int32)
            for i, bbox in zip(cat_ids, bbox_results)
        ])
        if class_names is None:
            labels = [f'class: {i}' for i in labels]
        else:
            labels = [class_names[i] for i in labels]

        self._det_results = []
        for bbox, label in zip(bboxes, labels):
            self._det_result = dict(bbox=bbox, label=label)
            self._det_results.append(self._det_result)
        return self._det_results

    def read_camera(self, frame) -> None:
        ts_input = time.time()

        self._event_inference_done.clear()
        self._input_queue.append((ts_input, frame))

        if self._args.synchronous_mode:
            self._event_inference_done.wait()

        self._frame_buffer.put((ts_input, frame))

    def inference_detection(self) -> None:
        min_interval = 1.0 / self._args.inference_fps
        # _ts_last = None  # timestamp when last inference was done

        ts_input, frame = self._input_queue.popleft()

        # inference detection
        mmdet_results = []
        if self._args.enable_detection:
            with self._inference_detection_stop_watch.timeit('Det'):
                mmdet_results = inference_detector(self._det_model, frame)

        t_info = self._inference_detection_stop_watch.report_strings()
        self._det_result_queue.append((ts_input, frame, t_info, mmdet_results))

        # limit the inference FPS
        # _ts = time.time()
        # if _ts_last is not None and _ts - _ts_last < min_interval:
        #     time.sleep(min_interval - _ts + _ts_last)
        # _ts_last = time.time()

    def inference_pose(self) -> None:
        ts_input, frame, t_info, mmdet_results = self._det_result_queue.popleft()

        self._pose_results_list: List[Any] = []
        for model_info, self._pose_history in zip(self._pose_model_list,
                                                  self._pose_history_list):
            model_name = model_info['name']
            pose_model = model_info['model']
            cat_ids = model_info['cat_ids']
            self._pose_results_last = self._pose_history['self._pose_results_last']
            next_id = self._pose_history['next_id']

            with self._inference_pose_stop_watch.timeit(model_name):
                # process mmdet results
                bbox_thr = None
                if self._args.enable_detection:
                    bbox_thr = self._args.det_score_thr
                    self._det_results = self.process_mmdet_results(
                        mmdet_results,
                        class_names=self._det_model.CLASSES,
                        cat_ids=cat_ids)
                else:
                    h = frame.shape[0]
                    w = frame.shape[1]
                    self._det_results = [{'bbox': np.array([0, 0, w, h])}]

                # inference pose model
                dataset_name = pose_model.cfg.data['test']['type']
                self._pose_results, _ = inference_top_down_pose_model(
                    pose_model,
                    frame,
                    self._det_results,
                    bbox_thr=bbox_thr,
                    format='xyxy',
                    dataset=dataset_name)

                self._pose_results, next_id = get_track_id(
                    self._pose_results,
                    self._pose_results_last,
                    next_id,
                    use_oks=False,
                    tracking_thr=0.3,
                    use_one_euro=True,
                    fps=None)

                self._pose_results_list.append(self._pose_results)

                # update pose history
                self._pose_history['self._pose_results_last'] = self._pose_results
                self._pose_history['next_id'] = next_id

        t_info += self._inference_pose_stop_watch.report_strings()
        self._pose_result_queue.append((ts_input, t_info, self._pose_results_list))

        self._event_inference_done.set()

    def display(self) -> Any:
        # initialize result status
        ts_inference = None  # timestamp of the latest inference result
        fps_inference = 0.  # infenrece FPS
        t_delay_inference = 0.  # inference result time delay
        self._pose_results_list = []  # latest inference result
        t_info = []  # upstream time information (list[str])

        # initialize visualization and output
        sunglasses_img = None  # resource image for sunglasses effect
        text_color = (228, 183, 61)  # text color to show time/system information
        vid_out = None  # video writer

        # show instructions
        # print('Keyboard shortcuts: ')
        # print('"v": Toggle the visualization of bounding boxes and poses.')
        # print('"s": Toggle the sunglasses effect.')
        # print('"b": Toggle the bug-eye effect.')
        # print('"Q", "q" or Esc: Exit.')
        keyboard_input = cv2.waitKey(1)
        if keyboard_input in (27, ord('q'), ord('Q')):
            return None
        elif keyboard_input == ord('s'):
            self._args.sunglasses = not self._args.sunglasses
        elif keyboard_input == ord('b'):
            self._args.bugeye = not self._args.bugeye
        elif keyboard_input == ord('v'):
            self._args.vis_mode = (self._args.vis_mode + 1) % 3

        with self._display_stop_watch.timeit('_FPS_'):
            # acquire a frame from buffer
            ts_input, frame = self._frame_buffer.get()
            # input ending signal
            if ts_input is None:
                return None

            img = frame

            # get pose estimation results
            if len(self._pose_result_queue) > 0:
                _result = self._pose_result_queue.popleft()
                _ts_input, t_info, self._pose_results_list = _result

                _ts = time.time()
                if ts_inference is not None:
                    fps_inference = 1.0 / (_ts - ts_inference)
                ts_inference = _ts
                t_delay_inference = (_ts - _ts_input) * 1000

            # prepare keypoints
            poses_for_cameras: List[PosesForCamera] = []

            # visualize detection and pose results
            if len(self._pose_results_list) > 0:
                for model_info, pose_results in zip(self._pose_model_list,
                                                    self._pose_results_list):
                    pose_model = model_info['model']
                    bbox_color = model_info['bbox_color']

                    dataset_name = pose_model.cfg.data['test']['type']

                    # fill keypoints
                    pose_id: int = 0
                    poses: List[Pose] = []
                    dataset_info = pose_model.cfg.data['test'].get('dataset_info', None)
                    for results_list in self._pose_results_list:
                        for results in results_list:
                            confidence = 0.0
                            keypoints: Dict[str, Keypoint] = {}
                            keypoints_definitions: Dict[str, int] = dict()
                            for i, k in enumerate(results['keypoints']):
                                key_id = dataset_info.keypoint_info[i]["id"]
                                if i != key_id:
                                    logger.warning(f"mmpose: mismatch between keypoint index {i} and id {key_id}")
                                name = dataset_info.keypoint_info[i]["name"].upper()
                                name = re.sub('[\ \-]', '_', name)
                                keypoints_definitions[name] = i
                                keypoints[name] = Keypoint(
                                    confidence=k[2],
                                    part=name,
                                    position=[float(k[0]), float(k[1])]
                                )
                                confidence += k[2]

                            if len(keypoints) > 0:
                                confidence = confidence / len(keypoints)
                            else:
                                confidence = 0
                            poses.append(Pose(
                                confidence=confidence,
                                id=pose_id,
                                keypoints=keypoints,
                                keypoints_definitions=keypoints_definitions
                            ))

                            pose_id += 1

                    poses_for_cameras.append(PosesForCamera(poses=poses))

                    # show pose results
                    if self._args.vis_mode == 1:
                        img = vis_pose_result(
                            pose_model,
                            img,
                            pose_results,
                            radius=4,
                            thickness=2,
                            dataset=dataset_name,
                            kpt_score_thr=1e7,
                            bbox_color=bbox_color)
                    elif self._args.vis_mode == 2:
                        img = vis_pose_result(
                            pose_model,
                            img,
                            pose_results,
                            radius=4,
                            thickness=2,
                            dataset=dataset_name,
                            kpt_score_thr=self._args.kpt_thr,
                            bbox_color=bbox_color)

                    # sunglasses effect
                    if self._args.sunglasses:
                        if dataset_name in {
                                'TopDownCocoDataset',
                                'TopDownCocoWholeBodyDataset'
                        }:
                            left_eye_idx = 1
                            right_eye_idx = 2
                        elif dataset_name == 'AnimalPoseDataset':
                            left_eye_idx = 0
                            right_eye_idx = 1
                        else:
                            raise ValueError(
                                'Sunglasses effect does not support'
                                f'{dataset_name}')

            # update keypoints
            self._poses_for_cameras = poses_for_cameras

            # delay control
            if self._args.display_delay > 0:
                t_sleep = self._args.display_delay * 0.001 - (time.time() - ts_input)
                if t_sleep > 0:
                    time.sleep(t_sleep)
            t_delay = (time.time() - ts_input) * 1000

            # show time information
            t_info_display = self._display_stop_watch.report_strings()  # display fps
            t_info_display.append(f'Inference FPS: {fps_inference:>5.1f}')
            t_info_display.append(f'Delay: {t_delay:>3.0f}')
            t_info_display.append(
                f'Inference Delay: {t_delay_inference:>3.0f}')
            t_info_str = ' | '.join(t_info_display + t_info)
            cv2.putText(img, t_info_str, (20, 20), cv2.FONT_HERSHEY_DUPLEX,
                        0.3, text_color, 1)
            # collect system information
            sys_info = [
                f'RES: {img.shape[1]}x{img.shape[0]}',
                f'Buffer: {self._frame_buffer.qsize()}/{self._frame_buffer.maxsize}'
            ]
            if psutil_proc is not None:
                sys_info += [
                    f'CPU: {psutil_proc.cpu_percent():.1f}%',
                    f'MEM: {psutil_proc.memory_percent():.1f}%'
                ]
            sys_info_str = ' | '.join(sys_info)
            cv2.putText(img, sys_info_str, (20, 40), cv2.FONT_HERSHEY_DUPLEX,
                        0.3, text_color, 1)

            # save the output video frame
            if self._args.out_video_file is not None:
                if vid_out is None:
                    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
                    fps = self._args.out_video_fps
                    frame_size = (img.shape[1], img.shape[0])
                    vid_out = cv2.VideoWriter(self._args.out_video_file, fourcc, fps,
                                              frame_size)

                vid_out.write(img)

            # display
            return img

    def start(self) -> bool:
        """Start the mmpose deep learning model.
        :return: bool - success of start. Returns False if there is no valid
        image generator, model path, etc.
        """
        return True

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)

        if len(input_streams) == 0:
            return False

        tracked_images = []

        for stream in input_streams.values():
            if Channel.Type.COLOR not in stream.channel_types:
                return False

            image: np.array = stream.get_channels_by_type(Channel.Type.COLOR)[0].data

            self.read_camera(image)
            self.inference_detection()
            self.inference_pose()
            tracked_image = self.display()

            if tracked_image is not None:
                tracked_images.append(tracked_image)

        if len(tracked_images) > 0:
            self._tracked_images = tracked_images
            return True

        return False

    def get_output(self) -> List[Channel]:
        channels: List[Channel] = [
            Channel(
                type=Channel.Type.POSE_2D,
                name="keypoints",
                data=self._poses_for_cameras,
                metadata={}
            )
        ]

        if self._tracked_images:
            for index, tracked_image in enumerate(self._tracked_images):
                channels.append(Channel(
                    type=Channel.Type.COLOR,
                    name=f"pose_image_{index}",
                    data=tracked_image,
                    metadata={
                        "resolution": [tracked_image.shape[1], tracked_image.shape[0]]
                    }
                ))

        return channels

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        if not hasattr(mmcv_env, 'cv2'):
            logger.warning(f"mmpose: opencv not available in mmcv")
            return False

        if not hasattr(mmcv_env, 'torch'):
            logger.warning(f"mmpose: torch not available in mmcv")
            return False

        if not torch.cuda.is_available() and not torch.backends.cuda.is_built():
            logger.warning(f"torch: CUDA not available")
            return False
        else:
            logger.info(f"torch: CUDA {torch.version.cuda} available")

        torch_cuda_device_count = 0

        # Testing CUDA enabled device count with cv2.cuda.getCudaEnabledDeviceCount which throws errors while torch.cuda.device_count does not:
        cv2_cuda_enabled_device_count = 0
        try:
            cv2_cuda_enabled_device_count = cv2.cuda.getCudaEnabledDeviceCount()
        except cv2.error as e:
            logger.error(f"{e.msg}")
            if e.code == -217:
                # -217: Gpu API call
                if e.err.find("forward compatibility was attempted on non supported HW") != -1 or e.err.find("system has unsupported display driver / cuda driver combination") != -1:
                    logger.error(
                        f"This error may be due to a recent upgrade of Nvidia drivers, if so you may need to reboot your computer.")
                sys.exit(1)
        if cv2_cuda_enabled_device_count == 0:
            logger.warning(f"opencv: no CUDA devices detected")
            return False

        try:
            torch_cuda_device_count = torch.cuda.device_count()
            logger.info(f"torch: found {torch_cuda_device_count} device(s)")
        except:
            logger.error(f"torch: error")
        if torch_cuda_device_count == 0:
            logger.warning(f"torch: no CUDA devices detected")
            return False

        if not torch.backends.cudnn.enabled:
            logger.warning(f"torch: CUDNN not enabled")
            return False
        else:
            logger.info(f"torch: CUDNN {torch.backends.cudnn.m.version()} enabled")  # type: ignore

        return True

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return False
