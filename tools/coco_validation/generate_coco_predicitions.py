# Generate predictions for the coco 2017 validation set using the official
# OpenPose python wrapper.

# NOTE: Before running this script you must first download the COCO 2017
# Images and Annotations, and place them in a directory called `coco/` at the
# top-level LivePose directory.
# You can download the COCO data by running
# `./tools/coco_validation/download_coco_data.sh`
# from the top-level LivePose directory.

# Usage:
# `python3 tools/coco_validation/generate_coco_predictions.py`

import os
import sys

import cv2

try:
    sys.path.insert(0, '/usr/local/python')  # PyOpenPose is installed in this directory by default
    from openpose.pyopenpose import Datum, VectorDatum, WrapperPython, get_images_on_directory
except ImportError:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake of OpenPose and installed it?')


DEFAULT_MODEL_PATH = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../livepose/models/openpose"
))

JSON_OUTPUT_PATH = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../coco/coco_predictions.json"
))

COCO_VAL_DIR = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../coco/val2017"
))


op_params = {
    "model_folder": DEFAULT_MODEL_PATH,
    "face": False,
    "hand": False,
    "write_coco_json": JSON_OUTPUT_PATH,
    # This can be changed to any multiple of 16. 256 was the highest I got with
    # the System76 Nvidia GPU before getting an out of memory error.
    # Using -1x368 is recommended if you have the memory for it.
    "net_resolution": "-1x256"
}

op_wrapper = WrapperPython()
op_wrapper.configure(op_params)
op_wrapper.start()

# Run inference on each image in the COCO Validation set.
image_paths = get_images_on_directory(COCO_VAL_DIR)
for img_path in image_paths:
    # Split Image ID from filename (needed for COCO JSON output format).
    img_name = img_path.split('/')[-1]
    img_id = img_name.rstrip('0')[:img_name.rfind('.')]

    datum = Datum()
    image_to_process = cv2.imread(img_path)
    datum.cvInputData = image_to_process
    # Image ID needs to be manually set for OpenPose to generate the JSON
    # output correctly.
    datum.name = img_id

    op_wrapper.emplaceAndPop(VectorDatum([datum]))
