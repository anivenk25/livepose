# Evaluate neural net prediction results on the COCO 2017 Validation
# set using the official metrics. 

# NOTE: Before running this script you must first generate keypoint predictions
# for all images in the COCO 2017 Validation dataset. These predictions will
# be scored against the official groundtruth results.

# To generate the predictions, run the related script in this directory:
# `python3 tools/coco_validation/generate_coco_predictions.py`

# NOTE: You must also download the groundtruth results in order to score your
# predictions, if you haven't already. You can download them by running
# ./tools/coco_validation/download_coco_data.sh
# from the top-level LivePose directory

# Usage:
# `python3 tools/evaluate_coco_predictions.py`


import os

from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval


COCO_ANNOTATIONS_PATH = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../coco/annotations/person_keypoints_val2017.json"
))

POSE_RESULTS_PATH = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../../coco/coco_predictions.json"
))


coco_ground_truth = COCO(COCO_ANNOTATIONS_PATH)

pose_detections = coco_ground_truth.loadRes(POSE_RESULTS_PATH)

coco_eval = COCOeval(coco_ground_truth, pose_detections, 'keypoints')

coco_eval.evaluate()
coco_eval.accumulate()
coco_eval.summarize()
