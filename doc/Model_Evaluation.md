Model Evaluation
================

The first version of LivePose relied on the pre-trained
[OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)
model for pose estimation.
OpenPose was originally trained and evaluated using the
[COCO 2017 Keypoint Detection Task](https://cocodataset.org/#keypoints-2017).
To verify the accuracy and performance of LivePose's OpenPose implementation,
we've included several scripts to evaluate keypoint detections for the
COCO 2017 validation dataset.

## Metrics
COCO uses an "Average Precision" metric to measure performance on the keypoint
detection task. To read more about this metric and how it is calculated, see
the [COCO Website](https://cocodataset.org/#keypoints-eval).

## Usage
The scripts for running the evaluation are all found in
[tools/coco_validation/](tools/coco_validation/).

There are 3 steps to the evaluation process, outlined below.

### Downloading COCO Validation Data
In order to evaluate the models, you must first download the COCO validation
images and their annotations.

From the top-level LivePose directory:

```bash
./tools/coco_validation/download_coco_data.sh
```

This will download all of the required data to `./coco/` in the top-level  
LivePose directory.

If the `./coco/` directory doesn't exist already, it will be created.

The data will be stored in two-subdirectories: `val2017/` and `annotations/`
containing the actual validation images and their groundtruth keypoint
annotations, respectively.

**Note:** The validation data takes up about 1.6GB, make sure you have enough
space before downloading.

### Generating Predictions
Once you have the validation data, you can generate keypoint predictions
for all of the validation images.

From the top-level LivePose directory:

```bash
python3 tools/coco_validation/generate_coco_predictions.py
```

This will generate keypoint predictions for each image and store them in a JSON
file. By default the results will be written to
`./coco/coco_predictions.json`

## Evaluating Predictions
Finally you can evaluate the predictions generated by OpenPose.

From the top-level LivePose directory:
```bash
python3 tools/coco_validation/evaluate_coco_predictions.py
```

This will print results for all of the evaluation metrics used by COCO.

Example Output:
```
$ python3 tools/coco_validation/evaluate_coco_predictions.py
loading annotations into memory...
Done (t=0.25s)
creating index...
index created!
Loading and preparing results...
DONE (t=0.33s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *keypoints*
DONE (t=5.03s).
Accumulating evaluation results...
DONE (t=0.09s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.426
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.681
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.441
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.314
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.582
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 20 ] = 0.484
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets= 20 ] = 0.706
 Average Recall     (AR) @[ IoU=0.75      | area=   all | maxDets= 20 ] = 0.502
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets= 20 ] = 0.332
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets= 20 ] = 0.696
```
